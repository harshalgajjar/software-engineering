package code;
import java.util.*; 

public class market {
	
//	market class for creating markets like BSE, NSE, etc
	
//	contains two functions + constructor:
//	newOrder: to add an order to the market
//	transact: to simulate all possible trades possible in the market 
	
	static List<order> purchaseOrders = new ArrayList<order> ();
	static List<order> saleOrders = new ArrayList<order> ();
	
	static int nTransactions;
	static int amountFlowed;
	
	public market() {
		market.nTransactions=0;
		System.out.println("==================\nMarket Initialized\n==================\n");
	}
	
	public void newOrder(String action, String name, int cost, int quantity, String client) {
//		adds an order to purchaseOrders or saleOrders depending action == buy or sell 
		
		stock newStock = new stock(cost, quantity, name);
		order newOrder = new order(action, newStock, client);
		
		switch(action) {
			
		case "buy":
			purchaseOrders.add(newOrder);
			newOrder = purchaseOrders.get(purchaseOrders.size()-1);
			System.out.println("Buy  order placed: ("+"orderNo:["+
								newOrder.getOrderNumber()+", "+
								newOrder.getStock().getName()+", "+
								newOrder.getClient()+"], cost:"+
								newOrder.getStock().getCost()+", quantity:"+
								newOrder.getStock().getQuantity()+")");
	
			break;
		
		case "sell":
			saleOrders.add(newOrder);
			newOrder = saleOrders.get(saleOrders.size()-1);
			System.out.println("Sell  order placed: ("+"orderNo:["+
								newOrder.getOrderNumber()+", "+
								newOrder.getStock().getName()+", "+
								newOrder.getClient()+"], cost:"+
								newOrder.getStock().getCost()+", quantity:"+
								newOrder.getStock().getQuantity()+")");
		
			break;
		
		default:
			return;
			
		}
		
	}
	
	
	public void transact() {
//		executes all possible transactions/trades possible in the market
//		using the two lists: purchaseOrders and saleOrders 
		
		boolean transaction;
		
		do {
			transaction=false;
			
			for(int i=0; i<purchaseOrders.size();) {
			
				for(int j=0; j<saleOrders.size();) {
				
					order purchase = purchaseOrders.get(i);
					order sale = saleOrders.get(j); 
					
					if(purchase.getStock().getCost()>=sale.getStock().getCost() && purchase.getStock().getName().equals(sale.getStock().getName())) {

//						transaction possible  
						
						System.out.println("------------------\nTransaction/Trade\n------------------");
						nTransactions++;
						transaction=true;
						
						int quantity=Math.min(purchase.getStock().getQuantity(), sale.getStock().getQuantity());
						purchase.getStock().setQuantity(purchase.getStock().getQuantity()-quantity);
						sale.getStock().setQuantity(sale.getStock().getQuantity()-quantity);
						
						System.out.println("["+sale.getOrderNumber() + ", "+sale.getStock().getName()+", "+ sale.getClient()+"] sold "+quantity+" shares to ["+purchase.getOrderNumber() + ", "+purchase.getStock().getName()+", "+ purchase.getClient()+"] at "+sale.getStock().getCost()+" per share");
						
						if(purchase.getStock().isHollow()) {
							
//							purchase/buy order completed 
							
							System.out.println(" - ["+purchase.getOrderNumber() + ", "+purchase.getStock().getName()+", "+ purchase.getClient()+"] fully completed");
							purchaseOrders.remove(i);
						}else {
							i=Math.max(i-1, 0);
							System.out.println(" - [" +purchase.getOrderNumber() + ", "+purchase.getStock().getName()+", "+ purchase.getClient()+"] partially completed - quantity remaining (for purchasing):"+purchase.getStock().getQuantity());
						}
						
						if(sale.getStock().isHollow()) {
							
//							sale/sell order completed
							
							System.out.println(" - ["+sale.getOrderNumber() + ", "+sale.getStock().getName()+", "+ sale.getClient()+"] fully completed");
							saleOrders.remove(i);
						}else {
							i=Math.max(i-1, 0);
							System.out.println(" - [" +sale.getOrderNumber() + ", "+sale.getStock().getName()+", "+ sale.getClient()+"] partially completed - quantity remaining (for selling):"+sale.getStock().getQuantity());
						}
						
					}
					j++;
				}
				i++;
				
			}
		
		} while(transaction);
        
//		printing an overview
		
		System.out.println("------------------");
		System.out.println(nTransactions+" transactions completed so far");
		System.out.println("Purchase orders pending: "+purchaseOrders.size());
		System.out.println("Sale     orders pending: "+saleOrders.size());
		
	}
	
	
}
