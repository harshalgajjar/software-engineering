package code;
import java.util.Scanner;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;

public class main {
	
	public static void main(String[] args) { 
		
		market Market = new market();
	
//		try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\input.csv"))) {
		try (BufferedReader br = new BufferedReader(new FileReader("input.csv"))) {
			String line = br.readLine();
			
			while (line != null) { 
				String[] inputs = line.split(",");
				
				int time = Integer.parseInt(inputs[0]);
				String command = inputs[1];
				String client = inputs[2];
				String name = inputs[3];
				int quantity = Integer.parseInt(inputs[4]);
				int cost = Integer.parseInt(inputs[5]);
				
				switch(command) {
				case "S":					
					Market.newOrder("sell", name, cost, quantity, client);
					break;
					
				case "B":
					Market.newOrder("buy", name, cost, quantity, client);
					break;
				}
				
				Market.transact();
				line = br.readLine();
			}
			
		}catch (IOException ioe) {
            ioe.printStackTrace();
        }
		

		
//		CONSOLE INTERACTIVE VERSION
		
//		Scanner in = new Scanner(System.in);
//		boolean exit = false;
//		while(!exit) {
//			
//			int time = in.nextInt();
//			String command = in.next();
//
//			String name;
//			int cost;
//			int quantity;
//			String client;
//			
//			switch(command) {
//			case "S":
//				client = in.next();
//				name = in.next();
//				quantity = in.nextInt();
//				cost = in.nextInt();
//				
//				Market.newOrder("sell", name, cost, quantity, client);
//				break;
//				
//			case "B":
//				client = in.next();
//				name = in.next();
//				quantity = in.nextInt();
//				cost = in.nextInt();
//				
//				Market.newOrder("buy", name, cost, quantity, client);
//				break;
//				
//			case "exit":
//				exit=true;
//				break;
//			
//			default:
//				client = in.next();
//				name = in.next();
//				quantity = in.nextInt();
//				cost = in.nextInt();
//				
//				System.out.println("Input invalid");
//				
//				break;
//			}
//			
//			if(!exit) Market.transact();
//			
//		}
	        
		return;
	}
}
