package code;

public class stock {

//	stock class contains stock cost, name, and quantity 
	
	int cost;
	int quantity;
	String name;
	
	public stock(int cost, int quantity, String name) {

		this.cost=cost;
		this.quantity=quantity;
		this.name = name;
	
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isHollow() {
		return this.quantity==0;
	}
	
}
