package code;

public class order {

//	order class contains stock, client, orderNumber, desired action (buy/sell)
	
	stock stock;
	String client;

	boolean forPurchase;
	boolean forSale;
	
	int time;
	
	static int nOrders=0;	
	int orderNumber;
	
	public stock getStock() {
		return stock;
	}

	public void setStock(stock stock) {
		this.stock = stock;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public order(String action, stock stock, String client) {
		
		switch(action) {
		case "buy":
			this.forPurchase=true;
			this.forSale=false;
			break;
		case "sell":
			this.forPurchase=false;
			this.forSale=true;
			break;
		default:
			return;
		}
		
		this.stock = stock;
		this.client = client;
		
		this.orderNumber=order.nOrders;
		
		order.nOrders+=1;
		
	}
	
	public boolean purchase(int quantity) {
		if(!this.forPurchase) return false;
		if(quantity>this.stock.getQuantity()) return false;
		
		this.stock.setQuantity(this.stock.getQuantity()-quantity);
		return true;
	}
	

	public boolean sell(int quantity) {
		if(!this.forSale) return false;
		if(quantity>this.stock.getQuantity()) return false;
		
		this.stock.setQuantity(this.stock.getQuantity()-quantity);
		return true;
	}
	
}
